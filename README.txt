README.txt
----------
Collapse Views filters and make them expandable on demand. Working with AJAX pager. Currently general for all views without choice.

INSTALLATION
------------
1. Download and enable this module:
   Copy views_collapsible_filters directory to /sites/all/modules (for example).
   All views are collapsed by default now.

CONFIGURATION
------------
No configuration yet.

AUTHOR / MAINTAINER
-----------------
- Julian Pustkuchen (http://Julian.Pustkuchen.com)
- Thomas Frobieter (http://blog.frobieter.de)