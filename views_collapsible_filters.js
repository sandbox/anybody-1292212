Drupal.views_collapsible_filters = {};

/**
 * Initialize collapsible filters.
 * Hide filters and make them toggleable.
 */
Drupal.views_collapsible_filters.init = function() {
  $('.view-filters').hide();
  $('.view-filters').before($('<a id="btn-views-filters" class="btn" href="#" title="' + Drupal.t("Show filters") + '">' + Drupal.t("Search & Filter") + '</a>'));
  $('a#btn-views-filters').click(function() {
	Drupal.views_collapsible_filters.toggle();
	return false;
  });
}

/**
 * Open the filters.
 */
Drupal.views_collapsible_filters.open = function() {
  // Add class when open
  $('.view-filters').show();
  $("a#btn-views-filters").addClass("open", 0);
}

/**
 * Close the filters.
 */
Drupal.views_collapsible_filters.close = function() {
  // Add class when open
  $('.view-filters').hide();
  $("a#btn-views-filters").removeClass("open", 0);
}

/**
 * Toggle the filters.
 */
Drupal.views_collapsible_filters.toggle = function() {
  $('.view-filters').toggle('fast', function() {
    $("a#btn-views-filters").toggleClass("open", 0);
  });
}

/**
 * Execute collapsible filters on document ready.
 */
Drupal.behaviors.views_collapsible_filters = function() {
  Drupal.views_collapsible_filters.init();	
}